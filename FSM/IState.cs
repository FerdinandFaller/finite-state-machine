namespace FF.FSM
{
    public interface IState
    {
        IFiniteStateMachine FiniteStateMachine { get; }

        void OnStateChanged(IState oldState, IState newState);
    }
}