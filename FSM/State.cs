namespace FF.FSM
{
    public abstract class State : IState
    {
        public IFiniteStateMachine FiniteStateMachine { get; private set; }


        protected State(IFiniteStateMachine fsm)
        {
            FiniteStateMachine = fsm;

            FiniteStateMachine.StateChangedEvent -= OnStateChanged;
            FiniteStateMachine.StateChangedEvent += OnStateChanged;
        }

        public abstract void OnStateChanged(IState oldState, IState newState);
    }
}