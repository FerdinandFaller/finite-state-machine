using System;

namespace FF.FSM
{
    public class FiniteStateMachine : IFiniteStateMachine
    {
        public IState CurrentState { get; private set; }

        public event Action<IState, IState> StateChangedEvent;


        public virtual void ChangeState(IState state)
        {
            var oldState = CurrentState;
            CurrentState = state;

            if(StateChangedEvent != null && oldState != state) StateChangedEvent(oldState, state);
        }
    }
}