﻿using System;

namespace FF.FSM
{
    public interface IFiniteStateMachine
    {
        IState CurrentState { get; }
        event Action<IState, IState> StateChangedEvent;

        void ChangeState(IState state);
    }
}
