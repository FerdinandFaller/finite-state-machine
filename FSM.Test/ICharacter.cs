using System;

namespace FF.FSM.Test
{
    public interface ICharacter
    {
        int Health { get; set; }

        event Action<int> HealthChangedEvent;
    }
}