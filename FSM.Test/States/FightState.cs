using System;

namespace FF.FSM.Test.States
{
    public class FightState : State
    {
        private static FightState _instance;

        public static FightState GetInstance(IFiniteStateMachine fsm)
        {
            return _instance ?? (_instance = new FightState(fsm));
        }

        private FightState(IFiniteStateMachine fsm) : base(fsm)
        {
        }

        public override void OnStateChanged(IState oldState, IState newState)
        {
            if (newState != this) return;

            Console.WriteLine("Fight!");
        }
    }
}