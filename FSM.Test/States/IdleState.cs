using System;

namespace FF.FSM.Test.States
{
    public class IdleState : State
    {
        private readonly int _characterStartHealth;

        private static IdleState _instance;
        public static IdleState GetInstance(IFiniteStateMachine fsm, ICharacter character)
        {
            return _instance ?? (_instance = new IdleState(fsm, character));
        }

        private IdleState(IFiniteStateMachine fsm, ICharacter character) : base(fsm)
        {
            _characterStartHealth = character.Health;

            character.HealthChangedEvent -= OnCharacterHealthChanged;
            character.HealthChangedEvent += OnCharacterHealthChanged;
        }

        public override void OnStateChanged(IState oldState, IState newState)
        {
            if (newState != this) return;

            Console.WriteLine("Idle...");
        }

        private void OnCharacterHealthChanged(int newHealth)
        {
            if (newHealth < _characterStartHealth)
            {
                Console.WriteLine("Lost Health!");

                FiniteStateMachine.ChangeState(FightState.GetInstance(FiniteStateMachine));
            }
        }
    }
}