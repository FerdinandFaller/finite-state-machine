using System;

namespace FF.FSM.Test
{
    public class TestCharacter : ICharacter
    {
        private int _health;
        public int Health
        {
            get { return _health; }
            set
            {
                var oldHealth = _health;
                _health = value;

                if (oldHealth != _health && HealthChangedEvent != null) HealthChangedEvent(_health);
            }
        }

        public event Action<int> HealthChangedEvent;


        public TestCharacter()
        {
            Health = 10;
        }
    }
}