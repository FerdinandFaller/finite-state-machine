﻿using System;
using FF.FSM.Test.States;

namespace FF.FSM.Test
{
    class Program
    {
        static void Main()
        {
            var c = new TestCharacter();

            var fsm = new FiniteStateMachine();
            var s = IdleState.GetInstance(fsm, c);
            fsm.ChangeState(s);

            c.Health = 5;

            Console.Read();
        }
    }
}
